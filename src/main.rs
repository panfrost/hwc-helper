/*
 * Copyright © 2021-2022 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: MIT
 */

use std::{
    fs::File,
    io::{stdout, BufReader, Write},
    str::FromStr,
};

#[macro_use]
extern crate serde_derive;

mod mali;
use mali::*;

const LICENSE_HEADER: &str = r#"<!--
Copyright © 2017-2020 ARM Limited.
Copyright © 2021-2022 Collabora, Ltd.
Author: Antonio Caggiano <antonio.caggiano@collabora.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next
paragraph) shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->
"#;

const ID_OFFSET: usize = "ARM_Mali-".len();
const ID_LEN: usize = 4;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename = "metrics")]
struct Metrics {
    pub id: String,
    #[serde(rename = "category", default)]
    pub categories: Vec<Category>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename = "category")]
struct Category {
    pub name: String,
    pub per_cpu: String,
    #[serde(rename = "event", default)]
    pub events: Vec<Event>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Event {
    pub offset: Option<usize>,
    pub advanced: Option<String>,
    pub counter: String,
    pub title: String,
    pub name: String,
    pub description: String,
    pub multiplier: Option<u32>,
    pub units: String,
}

fn main() {
    let xml_dir_path = "xml";
    let xml_dir = std::fs::read_dir(xml_dir_path)
        .expect(&format!("Failed to read {} directory", xml_dir_path));

    for xml_file in xml_dir.into_iter().filter(|dir| {
        dir.as_ref()
            .unwrap()
            .file_name()
            .to_str()
            .unwrap()
            .ends_with(".xml")
    }) {
        let xml_entry = xml_file.as_ref().unwrap();
        let xml_name = xml_entry.file_name().into_string().unwrap();
        let xml_path = format!("{}/{}", xml_dir_path, xml_name);

        print!("Processing {}", xml_path);
        stdout().flush().expect("Failed to flush to stdout");

        let f = File::open(&xml_path).expect("Failed to open xml file");

        let r = BufReader::new(f);

        let mut categories: Vec<Category> = quick_xml::de::from_reader(r).expect("Failed to read");

        categories.sort_by_key(|category| {
            get_category_index(category).expect("Failed to sort categories")
        });

        let id = String::from(&categories[0].events[0].counter[ID_OFFSET..ID_OFFSET + ID_LEN]);

        add_offset(&mut categories).expect("Failed to add offset");

        trim_names(&mut categories).expect("Failed to trim names");

        let metrics = Metrics { id, categories };

        let out_dir = format!("{}/out", xml_dir_path);
        if std::fs::read_dir(&out_dir).is_err() {
            std::fs::create_dir(&out_dir)
                .expect(&format!("Failed to create output dir at {}", out_dir));
        }

        let xml_string = quick_xml::se::to_string(&metrics).expect("Failed to serialize");
        let xml_name = xml_name
            .strip_prefix("events-Mali-")
            .expect("Failed to strip 'events-Mali-' prefix");
        let xml_name = xml_name
            .strip_suffix("_hw.xml")
            .expect("Failed to strip '_hw.xml' suffix");
        let out_path = format!("{}/out/{}.xml", xml_dir_path, &xml_name);
        write_pretty_xml(&xml_string, &out_path);
    }
}

/// Prettify XML by adding proper new lines and indentation
/// See: https://gist.github.com/lwilli/14fb3178bd9adac3a64edfbc11f42e0d
pub fn write_pretty_xml(xml_string: &str, out_path: &str) {
    let mut buf = Vec::new();

    let mut reader = quick_xml::Reader::from_str(xml_string);
    reader.trim_text(true);

    let fw = File::create(out_path).unwrap();
    let mut writer = quick_xml::Writer::new_with_indent(fw, ' ' as u8, 4);
    writer
        .write(LICENSE_HEADER.as_bytes())
        .expect("Failed to write license header");

    loop {
        let ev = reader.read_event(&mut buf);

        match ev {
            Ok(quick_xml::events::Event::Eof) => break, // exits the loop when reaching end of file
            Ok(event) => writer.write_event(event),
            Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
        }
        .expect("Failed to parse XML");

        // If we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
        buf.clear();
    }

    println!("\t-> ./{}", out_path);
}

fn trim_names(categories: &mut Vec<Category>) -> Result<(), String> {
    for category in categories.iter_mut() {
        let trimmed = match category.name.strip_prefix("Mali ") {
            Some(t) => t,
            None => return Err(String::from("Failed to strip Mali prefix")),
        };
        category.name = String::from(trimmed);

        for event in category.events.iter_mut() {
            let underscore_len = 1;
            let trimmed = &event.counter[ID_OFFSET + ID_LEN + underscore_len..];
            event.counter = String::from(trimmed);

            let trimmed = match event.title.strip_prefix("Mali ") {
                Some(t) => t,
                None => return Err(String::from("Failed to strip Mali prefix")),
            };
            event.title = String::from(trimmed);
        }
    }

    Ok(())
}

fn add_offset(categories: &mut Vec<Category>) -> Result<(), String> {
    for (category_id, category) in categories.iter_mut().enumerate() {
        for event in category.events.iter_mut() {
            let counter_name = &event.counter["ARM_Mali-".len()..];

            let underscore = counter_name.find("_").unwrap();
            let product_name = counter_name[..underscore].to_uppercase();

            if let Ok(product_id) = ProductId::from_str(&product_name) {
                let product = PRODUCTS
                    .iter()
                    .find(|p| p.product_id == product_id)
                    .expect(&format!("Failed to find product with id {:?}", product_id));

                for (mut offset, name) in product.counter_names.iter().enumerate() {
                    if counter_name == *name {
                        if category_id == CounterBlock::Shader as usize {
                            offset -= BLOCK_SIZE * 2
                        } else if category_id == CounterBlock::Tiler as usize {
                            offset -= BLOCK_SIZE
                        } else if category_id == CounterBlock::Mmu as usize {
                            offset -= BLOCK_SIZE * 3
                        }
                        event.offset = Some(offset);
                    }
                }
            } else {
                return Err(format!(
                    "Failed to find product id from name {}",
                    product_name
                ));
            }
        }
    }

    Ok(())
}

fn get_category_index(category: &Category) -> Result<usize, String> {
    match category.name.as_str() {
        "Mali Job Manager" => Ok(0),
        "Mali Tiler" => Ok(1),
        "Mali Memory System" | "Mali L2 Cache" => Ok(2),
        "Mali Shader Core" => Ok(3),
        _ => Err(format!("Unsupported category name {}", category.name)),
    }
}
